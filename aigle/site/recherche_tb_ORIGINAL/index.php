<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Recherche terrain et bâtiment</title>
<style type="text/css">

</style>

<link rel="stylesheet" type="text/css" href="search.css">
</head>

<body>

<?php
	if ($_POST) {
		$search_locaux=$_POST['search_locaux'];
		$search_terrains=$_POST['search_terrains'];
		$code_postale=$_POST['code_postale'];
		$commune=$_POST['commune'];
		$superficie_min=$_POST['superficie_min'];
		$superficie_max=$_POST['superficie_max'];
	}
?>


<form method="post" action="#">

	<div style="background:#f5f5f5; border:1px solid #e5e5e5; width: 100%; padding:10px; display: block; min-height: 210px; max-width:900px; margin: 0 auto;">
	<p>Je recheche un :</p>
		<div style="width:48%; display:inline-block; margin:3px;">
			<label>
			<input type="checkbox" name="search_locaux" value="locaux" id="search_locaux" <?php if (!empty($search_locaux)) echo "checked";?> >
	    	Local
	    	</label>
	    </div>
	    <div style="width:48%; display:inline-block; margin:3px;">
			<label>
		        <input type="checkbox" name="search_terrains" value="terrains" id="search_terrains" <?php if (!empty($search_terrains)) echo "checked";?> >
		        Terrain
		    </label>
		</div>  
		<div style="width:48%; display:inline-block; margin:3px;">
		  	<label for="textfield">Code postal</label>
		  	<input type="text" name="code_postale" id="search_zip" value="<?php echo $code_postale; ?>"/>
		</div>
		<div style="width:48%; display:inline-block; margin:3px;">
			  <label for="textfield">Commune</label>
			  <input type="text" name="commune" id="search_commune" value="<?php echo $commune; ?>"/>
		</div>
		<div style="width:48%; display:inline-block; margin:3px;">
			  <label for="textfield">Superficie minimum m<sup>2</sup></label>
			  <input type="text" name="superficie_min" id="search_minSup" value="<?php echo $superficie_min; ?>"/>
		</div>
		<div style="width:48%; display:inline-block; margin:3px;"> 
		  <label for="textfield">Superficie maximum m<sup>2</sup></label>
		  <input type="text" name="superficie_max" id="search_maxSup" value="<?php echo $superficie_max; ?>"/>
		</div>
		
		<input type="submit" value="Recherche" id="search_searchButton" >
		  
  </div>
</form>

<?php
if ($_POST) {

	$json_url = "json/terrain.json";
	$json = file_get_contents($json_url);
	$json = mb_convert_encoding($json, 'HTML-ENTITIES', "UTF-8"); // convert encoding UTF-8

	$data = json_decode($json, TRUE);

	$data_ids = array();
	foreach ($data as $key => $val) {
		$data_ids[] = $key;
	}

	$en_location=searchForBoolean($data,'Location',true);
	$en_vente=searchForBoolean($data,'Vente',true);

	$filtered_search_locaux=$data_ids; 	// before filter : all properties are accepted
	$filtered_search_terrains=$data_ids; 	// before filter : all properties are accepted

	$filtered_code_postale=$data_ids; 	// before filter : all postal codes are accepted
	$filtered_commune=$data_ids; 		// before filter : all locations are accepted
	$filtered_superficie_min=$data_ids; // before filter : all superficies are accepted
	$filtered_superficie_max=$data_ids; // before filter : all superficies are accepted
	
	if( isset($_POST['search_locaux']) ) {
		if( !empty($_POST['search_locaux']) && empty($_POST['search_terrains'])) {
			$filtered_search_locaux=searchForValue($data,'Objet','Locaux');
		}
	}
	if( isset($_POST['search_terrains']) ) {
		if( !empty($_POST['search_terrains']) && empty($_POST['search_locaux'])) {
			$filtered_search_terrains=searchForValue($data,'Objet','Parcelle');
		}
	}
	if( isset($_POST['code_postale']) ) {
		if( !empty($_POST['code_postale']) ) {
			$filtered_code_postale=searchForValue($data,'Localite',$_POST['code_postale']);
		}
	}
	if( isset($_POST['commune']) ) {
		if( !empty($_POST['commune']) ) {
			$filtered_commune=searchForValue($data,'Localite',$_POST['commune']);
		}
	}
	if( isset($_POST['superficie_min']) ) {
		if( !empty($_POST['superficie_min']) ) {
			$filtered_superficie_min=searchForMinVal($data,'Superficie',$_POST['superficie_min']);
		}
	}
	if( isset($_POST['superficie_max']) ) {
		if( !empty($_POST['superficie_max']) ) {
			$filtered_superficie_max=searchForMaxVal($data,'Superficie',$_POST['superficie_max']);
		}
	}
	
	$result = array();
	$result = array_intersect($data_ids, $filtered_search_locaux, $filtered_search_terrains, $filtered_code_postale, $filtered_commune, $filtered_superficie_min, $filtered_superficie_max);	

?>
	<h2>Résultats</h2>

	<?php 
	if (!empty($result)) {
		?>
		<h3>A louer</h3>
		<?php 
		$result_en_location = array();
		$result_en_location = array_intersect($result,$en_location);
		if (!empty($result_en_location)) {
			?>
		<div class="responsive-table-line" style="margin:0px auto;max-width:100%;">
			<table class="table table-bordered table-condensed table-body-center">
				<thead>
					<tr>
						<th>Titre</th>
						<th>Superficie</th>
						<th>Localité</th>
						<th>Vente</th>
						<th>Location</th>
						<th>Objet</th>
					</tr>
				</thead>
					<tbody>
					<?php
					foreach ($result_en_location as $key => $val) {
					?>
						<tr>
							<td data-title="Titre"><a href="pdf/<?php echo $data[$key]['Id'];?>.pdf" target="_blank"><?php echo $data[$key]['Titre'];?></a></td>
							<td data-title="Superficie"><?php echo $data[$key]['Superficie'];?> m<sup>2</sup></td>
							<td data-title="Localité"><?php echo $data[$key]['Localite'];?></td>
							<td data-title="Vente"><div class="<?php echo ($data[$key]['Vente']==1 ? 'booltrue':'boolfalse');?>"></div></td>
							<td data-title="Location"><div class="<?php echo ($data[$key]['Location']==1 ? 'booltrue':'boolfalse');?>"></div></td>
							<td data-title="Objet"><?php echo $data[$key]['Objet'];?></td>
						</tr>
					<?php
					}
					?>
					</tbody>
				</table>
			</div>	
			<?php 
		} else {
		?>
			Aucun objet à louer correspond à votre recherche, si vous désirez plus de renseignements, n’hésitez pas à <a href="http://www.aigleregion.ch/contact/" title="contact" target="_blank">nous contacter</a>.
<?php
		} 
		?>

<h3>A vendre</h3>

		<?php 
		$result_en_vente = array();
		$result_en_vente = array_intersect($result,$en_vente);
		if (!empty($result_en_vente)) {
			?>
			<div class="responsive-table-line" style="margin:0px auto;max-width:100%;">
				<table class="table table-bordered table-condensed table-body-center">
					<thead>
						<tr>
							<th>Titre</th>
							<th>Superficie</th>
							<th>Localité</th>
							<th>Vente</th>
							<th>Location</th>
							<th>Objet</th>
						</tr>
					</thead>
					<?php
					foreach ($result_en_vente as $key => $val) {
					?>
					<tbody>
						<tr>
							<td data-title="Titre"><a href="pdf/<?php echo $data[$key]['Id'];?>.pdf" target="_blank"><?php echo $data[$key]['Titre'];?></a></td>
							<td data-title="Superficie"><?php echo $data[$key]['Superficie'];?> m<sup>2</sup></td>
							<td data-title="Localité"><?php echo $data[$key]['Localite'];?></td>
							<td data-title="Vente"><div class="<?php echo ($data[$key]['Vente']==1 ? 'booltrue':'boolfalse');?>"></div></td>
							<td data-title="Location"><div class="<?php echo ($data[$key]['Location']==1 ? 'booltrue':'boolfalse');?>"></div></td>
							<td data-title="Objet"><?php echo $data[$key]['Objet'];?></td>
						</tr>
					<?php
					}
					?>
					</tbody>
				</table>
			</div>	
			<?php 
		} else {
		?>
			Aucun objet à vendre correspond à votre recherche, si vous désirez plus de renseignements, n’hésitez pas à contacter la COREB
		<?php
		} 
		?>

		<?php
	} else {
	// pas de résultats
	?>
		Aucun objet ne correspond à votre recherche, si vous désirez plus de renseignements, n’hésitez pas à contacter la COREB
	<?php	
	}
}
?>

<?php 

	function searchForValue($array, $search_field, $search_value) {
		$keys = array();
		foreach ($array as $key => $val) {
			/*	// Case sensitive
			if (strpos($val[$search_field],$search_value) !== false) {
				$keys[] = $key;
			}
			*/	// Case insensitive
			if (strpos(strtolower($val[$search_field]),strtolower($search_value)) !== false) {
				$keys[] = $key;
			}
		}
	   return $keys;
	}	

	function searchForRange($array, $search_minfield, $search_minvalue, $search_maxfield, $search_maxvalue) {
		$keys = array();
		foreach ($array as $key => $val) {
			if (($val[$search_minfield] >= $search_minvalue) && ($val[$search_maxfield] <= $search_maxvalue)) {
				$keys[] = $key;
			}
		}
	   return $keys;
	}	

	function searchForMinVal($array, $search_field, $search_value) {
		$keys = array();
		foreach ($array as $key => $val) {
			if (($val[$search_field] >= $search_value)) {
				$keys[] = $key;
			}
		}
	   return $keys;
	}	

	function searchForMaxVal($array, $search_field, $search_value) {
		$keys = array();
		foreach ($array as $key => $val) {
			if (($val[$search_field] <= $search_value)) {
				$keys[] = $key;
			}
		}
	   return $keys;
	}	

	function searchForBoolean($array, $search_field, $search_value) {
		$keys = array();
		foreach ($array as $key => $val) {
			if ($val[$search_field] === $search_value) {
				$keys[] = $key;
			}
		}
	   return $keys;
	}	
?>

</body>
</html>