<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/Editing_wp-config.php Modifier
 * wp-config.php} (en anglais). C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

if (stripos($_SERVER['HTTP_HOST'], 'aigle-test.allmeo.com') !== false ) {
	define('DB_NAME', 'allmeocom22');
	define('DB_HOST', 'localhost');
	define('DB_USER', 'aigle');
	define('DB_PASSWORD', 'lastturnwhiteeagle');
}
elseif ($_SERVER['SERVER_ADDR'] == "127.0.0.1") {
	define('DB_NAME', 'aigle');
	define('DB_HOST', 'localhost');
	define('DB_USER', 'aigle');
	define('DB_PASSWORD', 'password');
}
else {
	define('DB_NAME', 'aigleregionch');
	define('DB_HOST', 'mysql.aigleregion.ch');
	define('DB_USER', 'aigle');
	define('DB_PASSWORD', 'mysteriouswildbox');
}


/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X,awE|IcWlZJLjsVdUd&waW,|_q:c?PUZFTyZX{c-.JWGx/F{w=-6QBN_qsuD^ab');
define('SECURE_AUTH_KEY',  '-~Fnr%}C%% d3Zfe-Wb{^Q%(bld`A0[aDI-T+!h}jRI(Wq6+x+nNw+c~]a@?h8#*');
define('LOGGED_IN_KEY',    's*DQq49^+)u.n73z)2kCX([D$NUTR(p0 E>pU<g#{?K,%1x_NN3psPnkgI6n_car');
define('NONCE_KEY',        '!G%Z} -:*KLvN)*+9rx3WR-I?G>e(HZ@A[A++Z_OjXExQM,m</)(cBjXk<B2a-U)');
define('AUTH_SALT',        'NCnQs?<e/aVP{g:i7ftx<F`<e{snB*_[oI0b-tRT)JzwL6<Q}gNng6HT4A-V|[ 6');
define('SECURE_AUTH_SALT', 'WDu<-+zn~if&SH0K<|qf5MEVR$P1&EF+|eJOV]yG<{GjWTWM8nW%SK^R-bo|a3+D');
define('LOGGED_IN_SALT',   'R$K)</%bYV/2.f6JDah(112|Hz-ORY_>4-n<zcFS0)T3(EEcF>3NQ^-.]GKqb&ug');
define('NONCE_SALT',       'KP|1J-^nwkN,+s`G$lROPNAg|QPxQu|e|!Mb4CiNs-|M&mIQa=o,o7`%fYZVITwt');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Langue de localisation de WordPress, par défaut en Anglais.
 *
 * Modifiez cette valeur pour localiser WordPress. Un fichier MO correspondant
 * au langage choisi doit être installé dans le dossier wp-content/languages.
 * Par exemple, pour mettre en place une traduction française, mettez le fichier
 * fr_FR.mo dans wp-content/languages, et réglez l'option ci-dessous à "fr_FR".
 */
define ('WPLANG', 'fr_FR');

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
