<?php 
/** 
 * @package WordPress 
 * @subpackage PdeRegion_Theme
 */ 
?>

<script type="text/javascript">
function doIframe(){
	o = document.getElementsByTagName('iframe');
	for(i=0;i<o.length;i++){
		if (/\bautoHeight\b/.test(o[i].className)){
			setHeight(o[i]);
			addEvent(o[i],'load', doIframe);
		}
	}
}
function setHeight(e){
	if(e.contentDocument){
		e.height = e.contentDocument.body.offsetHeight + 35;
	} else {
		e.height = e.contentWindow.document.body.scrollHeight;
	}
}
function addEvent(obj, evType, fn){
	if(obj.addEventListener)
	{
		obj.addEventListener(evType, fn,false);
		return true;
	} else if (obj.attachEvent){
		var r = obj.attachEvent("on"+evType, fn);
		return r;
	} else {
		return false;
	}
}
if (document.getElementById && document.createTextNode){
	addEvent(window,'load', doIframe);
}
</script>
	<div id="bottom">
	<span style="color: rgb(180,180,180); font-size: 6pt;">&copy; Aigle R&eacute;gion - design : <a href="http://www.allmeo.com">allmeo.com</a></span><br />
	<span style="color: rgb(100,100,100); font-size: 8pt; font-weight: bold;"><a href="?page_id=127">Plan du site</a></span>
	</div>
</div>
</div>
</body>
</html>