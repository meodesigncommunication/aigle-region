<?php
/**
 * @package WordPress
 * @subpackage PdeRegion_Theme
 */
 /*
Template Name: Sitemap
*/

get_header();
?>
<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_ledistrict.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt"><?php the_title(); ?></span></div>
<div id="page_wrapper">
<? get_sidebar(); ?>
<div id="content">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="breadcrumb" style="padding: 0px; margin: 0px;">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
	</div>
	<div id="titlePage"><?php the_title(); ?></div>
<ul class="sitemap">
<?php wp_list_pages('sort_column=ID&title_li='); ?>
<ul>

</div>
<?php endwhile; endif;
?></div><?
get_footer(); ?>
