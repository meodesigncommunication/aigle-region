<?php
/**
 * @package WordPress
 * @subpackage PdeRegion_Theme
 */
 /*
Template Name: Page contenu
*/

get_header();

if (in_array(6,get_post_ancestors($post)) OR $post->ID == 6) { /* Aigle Région */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_ledistrict.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Aigle R&eacute;gion</span></div>

<?
} elseif (in_array(8,get_post_ancestors($post)) OR $post->ID == 8) { /* Promotion économique */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_promotion.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Promotion &eacute;conomique</span></div>

<?
} elseif (in_array(9,get_post_ancestors($post)) OR $post->ID == 9) { /* District d'Aigle */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_ledistrict.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">District d'Aigle</span></div>

<?
} elseif (in_array(11,get_post_ancestors($post)) OR $post->ID == 11) { /* Contact */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_ledistrict.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Contact</span></div>

<?
}  elseif (in_array(229,get_post_ancestors($post)) OR $post->ID == 10) { /* Tourisme */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_av2020.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Tourisme</span></div>

<?
}  elseif (in_array(229,get_post_ancestors($post)) OR $post->ID == 1434) { /* Intranet */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_ledistrict.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Intranet</span></div>

<?
}
?>
<div id="page_wrapper">

	<? get_sidebar(); ?>

	<div id="content">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="breadcrumb">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
	</div><!-- breadcrumb -->
	<div id="titlePage"><span class="sharelinks"><?php
	if ($post->ID != 53) { /* Devenir membre */
		if( function_exists('meo_getShareLinks')) {
			 // FDD- echo meo_getShareLinks(get_permalink()); 
		} elseif( function_exists('ADDTOANY_SHARE_SAVE_KIT')) {
			ADDTOANY_SHARE_SAVE_KIT();
		}
		?><!--FDD-<a href="<?php the_permalink(); ?>&amp;article2pdf=1"><img src="<?php bloginfo('template_url'); ?>/images/icon_pdf.png" style="border:0px; vertical-align: top;" /></a>--><?
	}
	?></span><span class="textTitle"><?php
	if ($post->ID == 2076 && !empty($_GET)) {
		echo "bourse d'emploi";
	}
	else {
		the_title();
	}
	?></span></div><!-- titlePage -->
	<div style="border-bottom:1px dashed black; font-size: 10pt; line-height: 18px; text-align: justify;">
	<? if ($post->post_parent == 61) {
	?><table>
				<tr>
					<td style="width:300px"><strong>Pourcentage: </strong><? echo get_meta('pourcent'); ?></td><td style="width:300px"><strong>Nationalit&eacute;/Permis: </strong><? echo get_meta('natio'); ?></td>
				</tr>
				<tr>
					<td style="width:300px"><strong>Entreprise: </strong><? echo get_meta('entreprise'); ?></td><td style="width:300px"><strong>Adresse: </strong><? echo get_meta('adresse'); ?></td>
				</tr>
				<tr>
					<td style="width:300px"><strong>Formation requise: </strong><? echo get_meta('formation'); ?></td><td style="width:300px"><strong>Date d'engagement: </strong><? echo get_meta('dateStart'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>En ligne depuis: </strong><? echo meo_get_the_date(); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>Informations suppl&eacute;mentaires: </strong><? echo get_meta('info'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>En savoir plus: </strong>
					<? if (is_url(get_meta('plus'))) {
						?><a href="<?= get_meta('plus') ?>"><?= get_meta('plus'); ?></a>
					<? } elseif (is_anEmail(get_meta('plus'))) {
						?><a href="mailto:<?= get_meta('plus') ?>"><?= get_meta('plus'); ?></a>
					<? } elseif (get_meta('plus') == "pdf") {
						$attachments =& get_children("post_type=attachment&post_mime_type=application/pdf&post_parent=".$post->ID."");
						foreach($attachments as $attachment => $attachment_array) {
							$file = get_post($attachment);
							?><a href="<?= $file->guid ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_pdf.png" style="border:0px;" /></a><?
						}
					} else {
						echo get_meta('plus');
					}?>
					</td>
				</tr>
			</table>
	<?
	} elseif ($post->post_parent == 9999999) {
	?><table>
				<tr>
					<td style="width:300px"><strong>Date: </strong><? echo get_meta('date'); ?></td><td style="width:300px"><strong>Heure: </strong><? echo get_meta('heure'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>Description: </strong><? echo get_meta('descr'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>R&eacute;servation: </strong><? echo get_meta('reservation'); ?></td>
				</tr>
				<tr>
					<td style="width:300px" colspan="2"><strong>En savoir plus: </strong>
					<? if (is_url(get_meta('plusManif'))) {
						?><a href="<?= get_meta('plusManif') ?>"><?= get_meta('plusManif'); ?></a>
					<? } elseif (is_anEmail(get_meta('plusManif'))) {
						?><a href="mailto:<?= get_meta('plusManif') ?>"><?= get_meta('plusManif'); ?></a>
					<? } elseif (get_meta('plusManif') == "pdf") {
						$attachments =& get_children("post_type=attachment&post_mime_type=application/pdf&post_parent=".$post->ID."");
						foreach($attachments as $attachment => $attachment_array) {
							$file = get_post($attachment);
							?><a href="<?= $file->guid ?>"><img src="<?php bloginfo('template_url'); ?>/images/icon_pdf.png" style="border:0px;" /></a><?
						}
					} else {
						echo get_meta('plusManif');
					}?>
					</td>
				</tr>
			</table>
	<?
	}
	if(get_the_content() != ""){ the_content(); } ?>
	</div><!-- end of post -->
	<div><!-- Comments -->
	<?php comments_template(); ?>
	<? if(get_meta('signal') == "on"){ ?><a href="?page_id=324&page_inexacte=<? the_ID() ?>">Signaler une inexactitude</a>

	<? }
	endwhile; endif; ?>

	</div>

</div>



<?php get_footer(); ?>
