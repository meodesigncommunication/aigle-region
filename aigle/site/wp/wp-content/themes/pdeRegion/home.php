<?php
/**
 * @package WordPress
 * @subpackage PdeRegion_Theme
 */
 /*
Template Name: Home
*/

get_header();
if (date("Ymd") < date("Y")."0320" || date("Ymd") > date("Y")."1221") {
	?><div id="bannerHome" style="background-image:url(<?php echo bloginfo('template_url');?>/images/banners/home_hiver.jpg); background-repeat: no-repeat; background-color: white;"><?php
} elseif (date("Ymd") < date("Y")."0620" && date("Ymd") > date("Y")."0321") {
	?><div id="bannerHome" style="background-image:url(<?php echo bloginfo('template_url');?>/images/banners/home_printemps.jpg); background-repeat: no-repeat; background-color: white;"><?php
} elseif (date("Ymd") < date("Y")."0920" && date("Ymd") > date("Y")."0621") {
	?><div id="bannerHome" style="background-image:url(<?php echo bloginfo('template_url');?>/images/banners/home_ete.jpg); background-repeat: no-repeat; background-color: white;"><?php
} else {
	?><div id="bannerHome" style="background-image:url(<?php echo bloginfo('template_url');?>/images/banners/home_automne.jpg); background-repeat: no-repeat; background-color: white;"><?php
}
?>
	<div style="width:335px; height: 222px; padding-top:80px; padding-left: 625px"><ul class="linksHome"><span style="font-size: 25pt; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px;">Une organisation au service de sa région</span><li class="page_item"><a href="<?php echo home_url('/aigleregion/portrait/'); ?>">Portrait</a></li><li class="page_item"><a href="<?php echo home_url('/promotion-economique/'); ?>">Promotion économique</a></li><li class="page_item"><a href="<?php echo home_url('/tourisme/la-citav/'); ?>">La CITAV</a></li>
</ul><!-- ?php wp_list_pages('include=12,14,16,18&sort_column=ID&title_li=');? --></ul>
	</div>
</div>
<div id="contentHome">
	<div id="excerptHome">
		<div id="left">
		<?php $myposts = get_posts('numberposts=4&orderby=date&order=ASC&category=4');
		$i = 0;
		foreach($myposts as $post) :
			setup_postdata($post);
			$attachments =& get_children( 'post_type=attachment&post_mime_type=image&post_parent='.get_the_id());
			foreach($attachments as $attachment => $attachment_array) {
				$imagearray = wp_get_attachment_image_src($attachment, 'full', false);
				$imageURI = $imagearray[0];
				$imageID = get_post($attachment);
				$imageTitle = $imageID->post_title;
				$imageDescription = $imageID->post_content;
				$imageDescription2 = sanitize_title($imageDescription);
				if($imageDescription2 == 'image-principale'){
						$imgPrincipale =true;
						$uriImgPrincipale = $imageURI;
						$titleImgPrincipale = $imageTitle;
				}
			}
			if ($i == 0){
			?> <a href="<?php echo home_url( get_post_meta($post->ID, 'destination_url', true) ); ?>"> <?php
			echo "<img src=\"".$uriImgPrincipale."\" alt=\"".$titleImgPrincipale."\" />"; ?><br />
			<div id="colorBack" style="background-image:url(<?php bloginfo('template_url'); ?>/images/home_greenback_dark.jpg);"><span style="font-size: 13pt; font-weight: bold; filter: glow(color=#0000ff,strength=3); filter: dropshadow(color=#ffff00,offX=5,offY=5);"><?php the_title(); ?></span><br /><span style="font-size: 12pt;"><?php  echo get_post_meta($post->ID, 'sousTitre', true); ?></span></div>
			<div style="padding: 10px; height: 110px;"><?php the_content(); ?></div></a>
		</div>
			<?php } elseif($i == 1){ ?>
		<div id="right">
			<a href="<?php echo home_url( get_post_meta($post->ID, 'destination_url', true) ); ?>"> <?php echo "<img src=\"".$uriImgPrincipale."\" alt=\"".$titleImgPrincipale."\" />"; ?><br />
			<div id="colorBack" style="background-image:url(<?php bloginfo('template_url'); ?>/images/home_greenback_light.jpg);"><span style="font-size: 13pt; font-weight: bold;"><?php the_title(); ?></span><br /><span style="font-size: 12pt;"><?php  echo get_post_meta($post->ID, 'sousTitre', true); ?></span></div>
			<div style="padding: 10px; height: 110px;"><?php the_content(); ?></div></a>
		</div>
			<?php } elseif($i == 2){ ?>
		<div id="left">
			<a href="<?php echo get_post_meta($post->ID, 'destination_url', true); ?>" target="_blank"> <?php echo "<img src=\"".$uriImgPrincipale."\" alt=\"".$titleImgPrincipale."\" />"; ?><br />
			<div id="colorBackSmall" style="background-image:url(<?php bloginfo('template_url'); ?>/images/home_greenback_light.jpg);"><span style="font-size: 13pt; font-weight: bold;"><?php the_title(); ?></span><br /><span style="font-size: 12pt;"><?php  echo get_post_meta($post->ID, 'sousTitre', true); ?></span></div></a>
		</div>
			<?php } else{ ?>
		<div id="right">
			<a href="<?php echo home_url( get_post_meta($post->ID, 'destination_url', true) ); ?>"> <?php echo "<img src=\"".$uriImgPrincipale."\" alt=\"".$titleImgPrincipale."\" />"; ?><br />
			<div id="colorBackSmall" style="background-image:url(<?php bloginfo('template_url'); ?>/images/home_greenback_dark.jpg);"><span style="font-size: 13pt; font-weight: bold;"><?php the_title(); ?></span><br /><span style="font-size: 12pt;"><?php  echo get_post_meta($post->ID, 'sousTitre', true); ?></span></div></a>
		</div>
		<?php }
			$i++;
			endforeach;
		?>
	</div>
	<div id="actuHome">
<?php
// Post 5 = Actualites
$mypost =& get_children("post_parent=5&numberposts=1&orderby=menu_order&order=ASC&post_type=page");
$rss_icon = includes_url('images/rss.png');
foreach($mypost as $post):
	setup_postdata($post);
	$feed_url = get_bloginfo('url') . '/feed?descendant_of=5';
	?>
	<div style="border-top: 1px dashed rgb(100,100,100); border-bottom: 1px dashed rgb(100,100,100); width:280px; text-transform: uppercase; color: rgb(213,27,50); font-size: 12pt; padding-top: 2px; margin-bottom: 5px;"><span class="sharelinks"><a class='rsswidget' href='<?php echo $feed_url; ?>' title='<?php echo esc_attr(__('Syndicate this content')); ?>'><img width='14' height='14' src='<?php echo $rss_icon; ?>' alt='RSS' /></a></span><a href="<?php the_permalink(); ?>" style="color: rgb(213,27,50);">Actualités</a></div>
	<div style="text-align:justify"><?php the_advanced_excerpt('length=28&use_words=1');?></div>
<a href="<?php the_permalink(); ?>" class="postMore">+ d'infos</a><div style="clear: both;"></div>
<?php endforeach;

// Prochainement
$post = meo_getNextProchainmentPage();
//print "<pre>"; print_r($post); print "<hr>"; die;
if (! empty($post)) {
	setup_postdata($post);
	$feed_url = get_bloginfo('url') . '/feed?descendant_of=922,2303';
	?>
	<div style="border-top: 1px dashed rgb(100,100,100); border-bottom: 1px dashed rgb(100,100,100); width:280px; text-transform: uppercase; color: rgb(213,27,50); font-size: 12pt; padding-top: 2px; margin-top: 30px; margin-bottom: 5px;"><span class="sharelinks"><a class='rsswidget' href='<?php echo $feed_url; ?>' title='<?php echo esc_attr(__('Syndicate this content')); ?>'><img width='14' height='14' src='<?php echo $rss_icon; ?>' alt='RSS' /></a></span><a href="<?php echo home_url('/communication/manifestations/');?>"  style="color: rgb(213,27,50);">Prochainement</a></div>
	<div style="text-align:justify"><strong><?php the_title();?></strong><br /><?php the_advanced_excerpt('length=28&use_words=1');?></div>
	<a href="<?php echo home_url('/communication/manifestations/');?>" class="postMore">+ d'infos</a><?php
}

// Newsletter subscription
?><a href="<?php echo home_url('/contact/'); ?>">
<div style="border-top: 1px dashed rgb(100,100,100); border-bottom: 1px dashed rgb(100,100,100); width:280px; text-transform: uppercase; color: rgb(213,27,50); font-size: 12pt; padding-top: 2px; margin-top: 30px; margin-bottom: 5px;">
	Contactez-nous
</div>
</a></div><!-- actuHome -->
<?php
get_footer();
?>
