<?php 
/** 
 * @package WordPress 
 * @subpackage PdeRegion_Theme
 */ 
 /*
Template Name: Page interm&eacute;diaire
*/

get_header();

if (in_array(6,get_post_ancestors($post)) OR $post->ID == 6) { /* Aigle Région */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_ledistrict.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Aigle R&eacute;gion</span></div>

<?
} elseif (in_array(8,get_post_ancestors($post)) OR $post->ID == 8) { /* Promotion économique */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_promotion.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Promotion économique</span></div>

<?
} elseif (in_array(9,get_post_ancestors($post)) OR $post->ID == 9) { /* District d'Aigle */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_ledistrict.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">District d'Aigle</span></div>

<?
} elseif (in_array(11,get_post_ancestors($post)) OR $post->ID == 11) { /* Contact */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_ledistrict.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Contact</span></div>

<?
}  elseif (in_array(229,get_post_ancestors($post)) OR $post->ID == 10) { /* Tourisme */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_av2020.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Tourisme</span></div>

<?
}  elseif (in_array(229,get_post_ancestors($post)) OR $post->ID == 1434) { /* Intranet */
?>

<div style="width: 930px; height: 61px; background-image: url(<?php bloginfo('template_url'); ?>/images/banners/header_ledistrict.jpg); color:white; padding-top: 90px; padding-left: 30px; text-shadow: rgba(0,0,0,0.6) 2px 3px 2px; margin-top:30px;"><span style="font-size: 30pt">Intranet</span></div>

<?
}

$is_news = (in_array(5,get_post_ancestors($post)) OR $post->ID == 5);
?>
<div id="page_wrapper">
	
	<? get_sidebar(); ?>

	<div id="content">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="breadcrumb">
		<?php
		if(function_exists('bcn_display')){
			bcn_display();
		}
		?>
	</div>
	<div id="titlePage"><?php the_title(); ?></div>
	<? if(get_the_content() != ""){ ?><div style="border-bottom:1px dashed black; font-size: 10pt; line-height: 18px; text-align: justify;"><?php the_content(); ?></div><? } ?>
	
	<?php
 		global $post;
 		$myposts = get_posts("post_parent=".$id."&post_type=page&numberposts=-1&orderby=menu_order&order=ASC");
		 foreach($myposts as $post) :
		 setup_postdata($post);
		 $attachments =& get_children( 'post_type=attachment&post_mime_type=image&post_parent='.get_the_id());
		 if(empty($attachments)) { ?>
		 <div class="postWithoutImg">
			<?php
			if ($is_news) {
				?><span class="sharelinks"><?php
				if( function_exists('meo_getShareLinks')) {
					echo meo_getShareLinks(get_permalink());
				} elseif( function_exists('ADDTOANY_SHARE_SAVE_KIT')) {
					ADDTOANY_SHARE_SAVE_KIT();
				}
				?></span><?php
			}
			?>
			<div id="excerptTitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div><span style="line-height: 18px; font-style: italic; font-size: 10pt; text-align: justify;">
			<? the_advanced_excerpt('length=40&use_words=1&exclude_tags=img,p,strong'); ?><a href="<?php the_permalink(); ?>" class="postMore">+ d'infos</a>
		</span>
		</div>
		<?php }else{ 
 			 	$imgPrincipale = false; 
				foreach($attachments as $attachment => $attachment_array) {
					$imagearray = wp_get_attachment_image_src($attachment, 'thumbnail', false);
					$imageURI = $imagearray[0];
					$imageID = get_post($attachment);
					$imageTitle = $imageID->post_title;
					$imageDescription = $imageID->post_content;
					$imageDescription2 = sanitize_title($imageDescription);			
					if($imageDescription2 == 'image-principale'){
							$imgPrincipale =true;
							$uriImgPrincipale = $imageURI;
							$titleImgPrincipale = $imageTitle;
							//echo "<img src=\"".$imageURI."\" alt=\"".$imageTitle."\" class= \"intro_post_image \"/>";
					}
				}
				if ($imgPrincipale){ ?>
					<div class="post">
					<a href="<?php the_permalink(); ?>">
					<?php echo "<img src=\"".$uriImgPrincipale."\" alt=\"".$titleImgPrincipale."\" class= \"intro_post_image \"/>"; ?>
					</a>
				<?php }else{ ?>
					<div class="postWithoutImg">
				<?php } ?>
			<div id="excerptTitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
			<div style="text-align: justify"><span style="line-height: 18px; font-style: italic; font-size: 10pt;"><? the_advanced_excerpt('length=40&use_words=1&exclude_tags=img,p,strong'); ?><a href="<?php the_permalink(); ?>" class="postMore">+ d'infos</a></span></div>
		</div>
		<?php }?>	
 		<?php endforeach; ?>	
	
	<?php endwhile; else: ?>
	<?php endif; ?>

	</div>
			
</div>
	
<?php get_footer(); ?>
