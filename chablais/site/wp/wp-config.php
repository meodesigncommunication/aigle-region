<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/Editing_wp-config.php Modifier
 * wp-config.php} (en anglais). C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

if (stripos($_SERVER['HTTP_HOST'], 'chablais-test.allmeo.com') !== false ) {
	define('DB_NAME', 'allmeocom23');
	define('DB_HOST', 'localhost');
	define('DB_USER', 'chablais');
	define('DB_PASSWORD', 'turnwhitelasteagle');
}
elseif ($_SERVER['SERVER_ADDR'] == "127.0.0.1") {
	define('DB_NAME', 'chablais');
	define('DB_HOST', 'localhost');
	define('DB_USER', 'chablais');
	define('DB_PASSWORD', 'password');
}
else {
	define('DB_NAME', 'chablaisch');
	define('DB_HOST', 'mysql.chablais.ch');
	define('DB_USER', 'wpchablais');
	define('DB_PASSWORD', 'judgemoneylikegrow');
}


/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/0-kvUe;f?Q+!C-%4.YAM)#[6=I[n`]Kh:_>.C(Pcc-AGodG#Yh-@Q~}>}cH,pt]');
define('SECURE_AUTH_KEY',  '%QSr)4}5%PINN}-Ng]Xnx!z}aIh|!W3k0>A;FtjO(nl:!IwP0eJY/NPFdHG ^9/&');
define('LOGGED_IN_KEY',    'Y]A}E {5yTC:%0Krzx3&r!_+~`HF>q{S=I.xKt_$Fh3=Om59aw+Q@hMkdFDp/p.+');
define('NONCE_KEY',        '_@O?1R1|[+0dv-W%(S}tB`FrRS_84!BEoPi//B/IM%lx5M`w7[Uj9I,f@Z{[uaCL');
define('AUTH_SALT',        'Y}}Q(afS0FLDv8u,h&a3ebJ. CR&2O!Xf(nkW@mK%y)eG6NYO0c 5kk=F_a/S/nx');
define('SECURE_AUTH_SALT', '-nC^$F<5pFIdKu_(~X$<|%zKhuNsc&4Ijt*wB,+$B/+|0s$9%&&!U8#z*T${E4UR');
define('LOGGED_IN_SALT',   'D;uq4&=fabM.2;Eqp.JC`[<|;8kSX[*4 &1oLS4(CY:`W<*X2VM@j|ZQM,Pysd9$');
define('NONCE_SALT',       'T=4RodEwE?ntPd7S2joWNQjeW..}mR/=~^TZ;0ch!.XM`}p!,ieG>X/~!}v.hV+#');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Langue de localisation de WordPress, par défaut en Anglais.
 *
 * Modifiez cette valeur pour localiser WordPress. Un fichier MO correspondant
 * au langage choisi doit être installé dans le dossier wp-content/languages.
 * Par exemple, pour mettre en place une traduction française, mettez le fichier
 * fr_FR.mo dans wp-content/languages, et réglez l'option ci-dessous à "fr_FR".
 */
define ('WPLANG', 'fr_FR');

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
