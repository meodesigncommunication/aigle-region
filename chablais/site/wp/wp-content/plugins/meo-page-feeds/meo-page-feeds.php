<?php
/*
Plugin Name: Page feeds
Version: 0.1
Author: meo
Author URI: http://www.allmeo.com/

Based on plugin by Marios Alexandrou ( http://www.mariosalexandrou.com/)
Specific to http://www.pays-denhaut.ch/

*/

/*** --------------------------------------------------------------------- ***
     Filters
 *** --------------------------------------------------------------------- ***/

// Ensure we save the "descendant_of" parameter, if it's there
add_filter('query_vars', 'meo_query_vars_descendant_of');
function meo_query_vars_descendant_of($vars) {
    $new_vars = array('descendant_of');
    $vars = $new_vars + $vars;
    return $vars;
}

// Modify the database query to bring back our pages, if necessary
add_filter('posts_where', 'meo_posts_where');
function meo_posts_where($var){
	global $table_prefix, $wp;

	if (!is_feed()){
		return $var;
	}

	$descendant_of = meo_get_descendant_of_parameter();
	if (empty($descendant_of)) {
		return $var;
	}

	$descendants = meo_get_descendants($descendant_of);
	$find = $table_prefix . "posts.post_type = 'post'";
	$replace_with = "( " . $table_prefix . "posts.post_type in ( 'post', 'page' ) and " . $table_prefix . "posts.id in (" . join(",", $descendants) . ") )";
	$var = str_replace($find, $replace_with, $var);

	//print_r($var);

	return $var;
}

/*** --------------------------------------------------------------------- ***
     Adjust last modified date, if necessary
 *** --------------------------------------------------------------------- ***/

add_filter('get_lastpostmodified', 'meo_get_lastpostmodified',10,2);

// We do this because is_feed is not set when calling get_lastpostmodified.
add_action('rss2_ns', 'meo_feed_true');
add_action('atom_ns', 'meo_feed_true');
add_action('rdf_ns', 'meo_feed_true');
add_action ('rss2_comments_ns', 'meo_feed_false');
add_action ('atom_comments_ns', 'meo_feed_false');

function meo_get_lastpostmodified($lastpostmodified, $timezone){
	global $meo_feed, $wpdb;

	if (!($meo_feed)){
		return $lastpostmodified;
	}

	$descendant_of = meo_get_descendant_of_parameter();
	if (empty($descendant_of)) {
		return $lastpostmodified;
	}

	$descendants = meo_get_descendants($descendant_of);

	// Based on get_lastpostmodified() in wp-includes/post.php
	$sql = "SELECT post_modified_gmt
			  FROM $wpdb->posts
			 WHERE post_status = 'publish'
			   AND post_type in ( 'post', 'page' )
			   AND " . $wpdb->posts  . ".id in (" . join(",", $descendants) . ")
			 ORDER BY post_modified_gmt DESC
			 LIMIT 1";

	$lastpostdate = $wpdb->get_var($sql);
	if ( $lastpostdate > $lastpostmodified ) {
			$lastpostmodified = $lastpostdate;
	}
	return $lastpostmodified;
}

function meo_feed_true(){
	global $meo_feed;
	$meo_feed = true;
}
function meo_feed_false(){
	global $meo_feed;
	$meo_feed = false;
}

/*** --------------------------------------------------------------------- ***
     Utility functions
 *** --------------------------------------------------------------------- ***/

function meo_get_descendants($post_ids) {
	global $wpdb;

	foreach ($post_ids as $post_id) {
		if (! is_numeric($post_id)) {
			// Invalid input
			return array();
		}
	}

	$result = array();
	$last_set = $post_ids;

	while (! empty($last_set)) {
		$result = array_merge($result, $last_set);
		$sql = "SELECT ID FROM $wpdb->posts WHERE post_parent in (" . join(", ", $last_set) . ")";
		$last_set =  $wpdb->get_col($sql);
	}

	return $result;
}

function meo_get_descendant_of_parameter() {
	global $wp;

	$valid_descendant_of_values = array(5, 922, 2303); // currently only actualities, 6a7 jeudi, manifestations

	if (! array_key_exists('descendant_of', $wp->query_vars)) {
		return array();
	}

	$result = array();
	$possible_ancestors = split(",", $wp->query_vars['descendant_of']);
	foreach ($possible_ancestors as $possible_ancestor) {
		if (in_array($possible_ancestor, $valid_descendant_of_values)) {
			$result[] = $possible_ancestor;
		}
	}

	return $result;
}


/*** --------------------------------------------------------------------- ***/

?>
