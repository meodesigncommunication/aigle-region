<?php
/**
 * @package WordPress
 * @subpackage PdeRegion_Theme
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/superfish.js" type="text/javascript"></script>
	<?php wp_head(); ?>

<script type="text/javascript">

    $(document).ready(function() {

    	$(".sf-menu li ul li ul").remove();

        $('ul.sf-menu').superfish({
            delay:       800,
            speed:       'slow',
            autoArrows:  false,
            dropShadows: false
        });
    });

</script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-40609975-4', 'chablais.ch');
	ga('send', 'pageview');
</script>
</head>
<body>
<div id="container">
	<div id="header">
		<div class="logo_pde">
			<a title="pdeRegion" href="<?php bloginfo('url'); ?>"><img class="logo" src="<?php bloginfo('template_url'); ?>/images/logo_chablais.jpg" alt="Aigle R&eacute;gion" /></a>
		</div>
		<div class="logo_vaud">
			<!-- a title="vaud" href="http://www.vaud.ch"><img class="logo" src="<?php bloginfo('template_url'); ?>/images/logo_vaud.jpg" alt="Vaud.ch" /></a -->
		</div>
		<div id="searchWP">
	    		<?php include_once('searchform.php'); ?>
		</div>
		<div id="menu">
			<div id="menuRoll">
				<ul class="sf-menu">
					<?php wp_list_pages('sort_column=menu_order&title_li=&exclude=266,324,499,592,2076');?>
				</ul>
			</div>
		</div>
	</div>
